const express = require('express')
const app = express()
const bodyParser = require('body-parser')

const options = {
    hostname: `westus.api.cognitive.microsoft.com`,
    port: 443,
    path: `/qnamaker/v2.0/knowledgebases/b49a4b5a-8a4e-4feb-9caa-604ea9a6ba82`,
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json',
        'Ocp-Apim-Subscription-Key': `ba0747d02f2a42ffbc0f0dd2cb1fba06`,
    }
}
let data = {
    "add": {
        "qnaPairs": [
            {
                "answer": "Hello, How can I help you?",
                "question": "jjjjjj"
            }
        ]
    }
}

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static('public'))


app.post('/update', function (req, res) {
    const request = require('request');
    const postData = {
        "add": {
            "qnaPairs": [
                {
                    "answer": req.body.answer,
                    "question": req.body.question
                }
            ]
        }
    }

    request({
        url: `https://westus.api.cognitive.microsoft.com/qnamaker/v2.0/knowledgebases/b49a4b5a-8a4e-4feb-9caa-604ea9a6ba82`,
        method: "PATCH",
        json: true,
        headers: {
            'Content-Type': 'application/json',
            'Ocp-Apim-Subscription-Key': `ba0747d02f2a42ffbc0f0dd2cb1fba06`,
        },
        body: postData
    }, function (error, response, body) {
        request({
            url: `https://westus.api.cognitive.microsoft.com/qnamaker/v2.0/knowledgebases/b49a4b5a-8a4e-4feb-9caa-604ea9a6ba82`,
            method: "PUT",
            headers:
            {
                'Ocp-Apim-Subscription-Key': `ba0747d02f2a42ffbc0f0dd2cb1fba06`,
            }
        }, function (error, response, body) { 
            console.log("PUT OK")
         })
    }
    )


    res.send('Hello World!')
})

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
})